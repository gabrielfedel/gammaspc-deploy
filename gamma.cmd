require gammaspc, 2.1
require iocStats, 3.1.16

epicsEnvSet("LOCATION", "ion-pump-ioc-ts2 172.16.110.43")
epicsEnvSet("IOCNAME", "GammaSPC")

iocshLoad("$(iocStats_DIR)/iocStats.iocsh", "IOCNAME=$(IOCNAME)")

# System 1 - First instance
epicsEnvSet("PREFIX", "TS2-010RFC:RFS01")
epicsEnvSet("SGNL", "VacPSCom-110")
epicsEnvSet("PORT", "GAMMA11")
epicsEnvSet("IP", "172.16.110.44:23")

iocshLoad $(gammaspc_DIR)/gammaSpce.iocsh

# System 1 - Second instance
epicsEnvSet("PREFIX", "TS2-010RFC:RFS01")
epicsEnvSet("SGNL", "VacPSCom-120")
epicsEnvSet("PORT", "GAMMA12")
epicsEnvSet("IP", "172.16.110.45:23")

iocshLoad $(gammaspc_DIR)/gammaSpce.iocsh

# System 2 - First instance
epicsEnvSet("PREFIX", "TS2-010RFC:RFS02")
epicsEnvSet("SGNL", "VacPSCom-110")
epicsEnvSet("PORT", "GAMMA21")
epicsEnvSet("IP", "172.16.110.46:23")

iocshLoad $(gammaspc_DIR)/gammaSpce.iocsh

# System 2 - Second instance
epicsEnvSet("PREFIX", "TS2-010RFC:RFS02")
epicsEnvSet("SGNL", "VacPSCom-120")
epicsEnvSet("PORT", "GAMMA22")
epicsEnvSet("IP", "172.16.110.47:23")

iocshLoad $(gammaspc_DIR)/gammaSpce.iocsh
